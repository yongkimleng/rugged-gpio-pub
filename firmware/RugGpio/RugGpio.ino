//
// Rugged GPIO module controller (primary)
// - target device: leonardo / atmega32u4

// Inputs
// D0, A3, A2, A1, A0, D15, D14, D16

// Outputs
// D1, D2, D3, D4, D5, D6,  D7,  D8

// Bandwidth allocation:
// 115200 bps, due to 2-byte words, effective bw 57600 bps
// dedicate 9600 bps to serial data, remainder 48000 bps, or 6000 Bps
// poll inputs at most 1khz: 1-2K Bps
// remainder: 4k Bps


#include "rg_uid.h"
#include "rg_serial.h"
#include "rg_io.h"

void setup()
{
  // Initialize UID
  rg_uid::init();

  // Initialize IO
  rg_io::init();

  // initialize virtual serial and wait for connectivity
  rg_serial::init();
}

uint8_t prev_input = 0x00;
uint8_t prev_input2 = 0x00;
uint8_t new_input = 0x00;
uint8_t new_input2 = 0x00;
uint32_t last_read_ts = 0;
uint32_t now;

void loop()
{
  rg_serial::parse_serial();
  
  if(!rg_serial::CONFIGURED) return;

  // read at most 1khz
  now = millis();
  if(last_read_ts != now) {
    last_read_ts = now;
    
    new_input = rg_io::read_input(rg_serial::TTLSERIAL);
    
    if(new_input != prev_input) {
      rg_serial::write_serial_input( new_input );
      prev_input = new_input;
    }
    
    if(rg_serial::READALL) {
      new_input2 = rg_io::read_input2(rg_serial::TTLSERIAL);
      
      if(new_input2 != prev_input2) {
        rg_serial::write_serial_input2( new_input2 );
        prev_input2 = new_input2;
      }
    }
  }

  // drain at most 1 char per iteration, fine for 9600bps
  rg_serial::relay_ttl_serial();
 
}
