#ifndef __RG_IO__
#define __RG_IO__


namespace rg_io {

  void init() {
    // Set all pins (0-17) to input first
    for(uint8_t i = 0; i < 18; ++i) {
      if(i == 14) continue;  // ignore 14
      pinMode(i, INPUT);
      digitalWrite(i, 0);
    }
    
    // Set A0-3 low too
    pinMode(A0, INPUT);  digitalWrite(A0, 0);
    pinMode(A1, INPUT);  digitalWrite(A1, 0);
    pinMode(A2, INPUT);  digitalWrite(A2, 0);
    pinMode(A3, INPUT);  digitalWrite(A3, 0);
  }

  // Configure D1-8 as inputs
  void configure_inputs()
  {
    for(uint8_t i = 1; i < 9; ++i) {
      pinMode(i, INPUT);
      digitalWrite(i, 0);
    }
  }

  // Configure D1-8 as outputs
  void configure_outputs()
  {
    for(uint8_t i = 1; i < 9; ++i) {
      pinMode(i, OUTPUT);
    }
  }

  // write byte to Outputs: D1, D2, D3, D4, D5, D6,  D7,  D8
  void write_output(uint8_t b, bool ttlSerial = false)
  {
    if(!ttlSerial) digitalWrite(1, (b & 0b00000001));
    digitalWrite(2, (b & 0b00000010));
    digitalWrite(3, (b & 0b00000100));
    digitalWrite(4, (b & 0b00001000));
    digitalWrite(5, (b & 0b00010000));
    digitalWrite(6, (b & 0b00100000));
    digitalWrite(7, (b & 0b01000000));
    digitalWrite(8, (b & 0b10000000));
  }

  // read from Inputs D0, A3, A2, A1, A0, D15, D14, D16
  uint8_t read_input(bool ttlSerial = false)
  {
    uint8_t output = 0;
    if(!ttlSerial) if(digitalRead(0))  output |= 0b00000001;
    if(digitalRead(A3)) output |= 0b00000010;
    if(digitalRead(A2)) output |= 0b00000100;
    if(digitalRead(A1)) output |= 0b00001000;
    if(digitalRead(A0)) output |= 0b00010000;
    if(digitalRead(15)) output |= 0b00100000;
    if(digitalRead(14)) output |= 0b01000000;
    if(digitalRead(16)) output |= 0b10000000;
    return output;
  }

  // read from Inputs D1, D2, D3, D4, D5, D6,  D7,  D8
  uint8_t read_input2(bool ttlSerial = false)
  {
    uint8_t output = 0;
    if(!ttlSerial) if(digitalRead(1)) output |= 0b00000001;
    if(digitalRead(2)) output |= 0b00000010;
    if(digitalRead(3)) output |= 0b00000100;
    if(digitalRead(4)) output |= 0b00001000;
    if(digitalRead(5)) output |= 0b00010000;
    if(digitalRead(6)) output |= 0b00100000;
    if(digitalRead(7)) output |= 0b01000000;
    if(digitalRead(8)) output |= 0b10000000;
    return output;
  }





  
};


#endif
