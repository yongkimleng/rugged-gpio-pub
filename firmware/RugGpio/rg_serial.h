#ifndef __RG_SERIAL__
#define __RG_SERIAL__

#include <Servo.h>
#include "rg_io.h"
#include "rg_uid.h"

namespace rg_serial {

  uint8_t READALL = 0;
  uint8_t TTLSERIAL = 0;
  uint8_t CONFIGURED = 0;

  Servo servo[8];

  void init() {
    
    Serial.begin(115200);
    while(!Serial);  // atmega32u4/leonardo, wait for serial to open
  }
  
  /*
    responses:
    0x10 0xNN - input changed
    0x11 0xNN - input2 changed (readall mode)
    0x12 0xNN - serial byte read
    0xFF 0xFF - error
  */
  void write_serial_error()           {  Serial.write(0xFF);  Serial.write(0xFF); }
  void write_serial_input(uint8_t b)  {  Serial.write(0x10);  Serial.write(b);    }
  void write_serial_input2(uint8_t b) {  Serial.write(0x11);  Serial.write(b);    }
  void write_serial_read(uint8_t b)   {  Serial.write(0x12);  Serial.write(b);    }

  // Write 8-byte device ID
  void write_serial_deviceid() {
    uint64_t deviceId = rg_uid::get();
    Serial.write(0x01);
    for(uint8_t i = 0; i < 8; ++i) {
      uint32_t resultant = deviceId >> (8 * i);
      Serial.write((uint8_t)resultant);
    }
  }

  // Relays 1 byte from ttl serial to virtual serial
  void relay_ttl_serial() {
    if(Serial1.available()) write_serial_read( Serial1.read() );
  }

  /* Protocol:
  
  deviceid
  req: 0x01 0x00
  response: 0x01 0xNN 0xNN 0xNN 0xNN 0xNN 0xNN 0xNN 0xNN
  
  configure and begin
  req: 0x02 0xNN configuration bits
  bit 0 - outputs enabled (disable readall mode)
  bit 1 - serials enabled
  response: -
  
  write output
  req: 0x03 0xNN where NN maps to output bits
  response: -
  
  write serial
  req: 0x04 0xNN where NN is byte to send serial
  response: -

  servo pwm output (1-2ms)
  req: 0x10-17 0xNN set servo output on 1-8, where NN is 0 to 200 (corresponds to 1000-2000ms), > 200 disables
  response: -
  */
  
  uint8_t parse_serial_cmd[2];
  void parse_serial()
  {
    // always read in twos
    while(Serial.available() >= 2) {
      parse_serial_cmd[0] = Serial.read();
      parse_serial_cmd[1] = Serial.read();
        
      switch( parse_serial_cmd[0] ) {
        case 0x01:
        {
          // get device id
          write_serial_deviceid();
          break;
        }
        
        case 0x02:
        {
          // configure
          if(parse_serial_cmd[1] & 0x01)
            READALL = 0;
          else
            READALL = 1;
            
          if(parse_serial_cmd[1] & 0x02)
            TTLSERIAL = 1;
          else
            TTLSERIAL = 0;
            
          // return inputs once on configure
          rg_serial::write_serial_input( rg_io::read_input(rg_serial::TTLSERIAL) );
    
          if(!READALL) {
            rg_io::configure_outputs();
          
          } else {
            rg_io::configure_inputs();

            // return input2 once
            rg_serial::write_serial_input2( rg_io::read_input2(rg_serial::TTLSERIAL) );
          }
          
          if(TTLSERIAL) {
            // normal serial : just that in READALL mode, you only recv not send
            Serial1.begin(9600);
          
          } else {
            Serial1.end();
          }

          CONFIGURED = 1;
          
          break;
        }
          
        case 0x03:
        {
          // write output
          if(!CONFIGURED || READALL) {
            write_serial_error();
            break;
          }
          
          rg_io::write_output( parse_serial_cmd[1], TTLSERIAL );
          break;
        }
          
        case 0x04:
        {
          // write TTL serial
          if(!CONFIGURED || READALL || !TTLSERIAL) {
            write_serial_error();
            break;
          }
          
          Serial1.write( parse_serial_cmd[1] );
          break;
        }

        case 0x10:
        case 0x11:
        case 0x12:
        case 0x13:
        case 0x14:
        case 0x15:
        case 0x16:
        case 0x17:
        {
          // set servo pwm
          if(!CONFIGURED || READALL) {
            write_serial_error();
            break;
          }
          
          uint8_t servoid = parse_serial_cmd[0] - 0x10;
          bool servoAttached = servo[servoid].attached();
          
          if(parse_serial_cmd[1] > 200 && servoAttached) {
            servo[servoid].detach();
          
          } else if(parse_serial_cmd[1] <= 200) {
            // Don't do this if TTL serial is active on D1
            if(servoid == 0 && TTLSERIAL) {
              break;
            }
            
            if(!servoAttached) {
              servo[servoid].attach(servoid + 1);
            }

            servo[servoid].writeMicroseconds(1000 + parse_serial_cmd[1] * 5);
          }
          break;
        }
        
        default:
        {
          write_serial_error();
          break;
        }
      }
    }
  }
  
};


#endif
