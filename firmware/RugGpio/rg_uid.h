#ifndef __RG_UID__
#define __RG_UID__

//
// Retrieve UID from atmel low-level
//

#include <avr/boot.h>

#define SIGRD 5

namespace rg_uid {

  uint8_t signature[3];
  const uint8_t signature_length = 3;
  
  uint8_t serial[10];
  const uint8_t serial_length = 10;
  
  void init() {
    uint8_t j = 0;
    for (uint8_t i = 0; i < 5; i += 2) {
      signature[j++] = boot_signature_byte_get(i);
    }

    j = 0;
    for (uint8_t i = 14; i < 24; i += 1) {
      serial[j++] = boot_signature_byte_get(i);
    }
  }

  // 64-bit UID would consist of 2 bytes signature and 6 bytes LSB of serial
  // not globally unique but, should be unique enough when using a number of similar controllers on the same system
  union Uid {
    uint64_t a;
    uint8_t b[8];
  };
  
  uint64_t get() {
    Uid uid;
    uid.b[7] = signature[2];
    uid.b[6] = signature[1];
    uid.b[5] = serial[5];
    uid.b[4] = serial[4];
    uid.b[3] = serial[3];
    uid.b[2] = serial[2];
    uid.b[1] = serial[1];
    uid.b[0] = serial[0];
    return uid.a;
  }
};

#endif
