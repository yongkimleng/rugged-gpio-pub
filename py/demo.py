from ruggpio import RugGpio
import time


gpio = RugGpio("COM16")

while not gpio.isInitialized():
    print("Waiting for init")
    time.sleep(0.5)

while True:
    for i in range(8):
        gpio.setOutput(1 << i)
        time.sleep(0.5)

    gpio.setOutput(0)
    
    for i in range(30):
        gpio.setPwm(7, i)
        time.sleep(0.1)

    gpio.setPwm(7, 255)
    
