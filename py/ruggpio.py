"""

Rug Gpio library

"""

import serial, threading, time

class RugGpio(object):
    """
    Members
    - portName
    - ser
    - listenThread
    - initialized
    - deviceId
    - inputValue
    """
    
    def __init__(self, portName):
        self.portName = portName

        self.ser = serial.Serial()
        self.ser.port = self.portName
        self.ser.baudrate = 115200
        self.ser.open()

        self.initialized = False

        self.deviceId = ""

        self.inputValue = 0

        self.lastOutputByte = 0

        # Start listening thread
        self.listenThread = threading.Thread(target=self.listener)
        self.listenThread.daemon = True
        self.listenThread.start()

    def isInitialized(self):
        return self.initialized

    def getDeviceId(self):
        return self.deviceId

    def getInputValue(self):
        return self.inputValue

    def getInputBit(self, bitPos):
        bitmask = 1 << bitPos
        if not self.inputValue & bitmask == 0x00:
            return True
        else:
            return False

    def listener(self):
        self.requestDeviceId()
        self.setConfigure()

        responseByte = 0
        payloadByte = 0

        while True:
            # read bytes repeatedly, sleep if there is none
            responseByte = self.readByte()

            if responseByte == 0x01:
                # device ID received, read 8 following bytes
                self.deviceId = ""
                for i in range(8):
                    b = self.readByte()
                    self.deviceId += hex(b)

                # we're initialized only afte deviceId is returned
                self.initialized = True

            elif responseByte == 0x10:
                # input changed
                self.inputValue = self.readByte()
                
            elif responseByte == 0x11:
                # input 2 changed
                payloadByte = self.readByte()
                # TODO implement in future
                print("Input 2 returned: ", payloadByte)
                
            elif responseByte == 0x12:
                # serial byte returned
                payloadByte = self.readByte()
                print("Serial byte returned: ", payloadByte)
                # TODO implement in future
                
            elif responseByte == 0xFF:
                payloadByte = self.readByte()

                if payloadByte != 0xFF:
                    print("Unknown sequence", responseByte, payloadByte)

                else:
                    print("Error response returned")

            else:
                # unknown/null
                print("Unknown response", responseByte)
                time.sleep(0.1)

    # Read a byte and convert into something more convenient
    def readByte(self):
        b = self.ser.read()
        return int.from_bytes(b, "big")

        

    # Protocol
    def requestDeviceId(self):
        seq = bytearray([ 0x01, 0x00 ])
        self.ser.write(seq)

    def setConfigure(self):
        # enable outputs, no serial
        seq = bytearray([ 0x02, 0x01 ])
        self.ser.write(seq)

    def setOutput(self, bitsByte):
        # sets outputs corresponding to input bits
        self.lastOutputByte = bitsByte
        seq = bytearray([ 0x03, bitsByte ])
        self.ser.write(seq)

    def setOutputBit(self, bitPos, enable):
        # sets a bit in output, uses last output byte
        bitmask = 1 << bitPos
        if enable == True:
            self.lastOutputByte = self.lastOutputByte | bitmask

        else:
            bitmask = 0xFF ^ bitmask
            self.lastOutputByte = self.lastOutputByte & bitmask
            
        seq = bytearray([ 0x03, self.lastOutputByte ])
        self.ser.write(seq)

    def setPwm(self, pin, value):
        # set pwn on pin 1-8 (mapped 0-7), value 0-200 (>200 to disable)
        command = 0x10 + pin
        seq = bytearray([ command, value ])
        self.ser.write(seq)

    
    



